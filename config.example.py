# -*- coding: utf-8 -*-
# !/usr/bin/env python3


infinitt_db_user = "spectra"
infinitt_db_pw = ""
# fqdn of the database server
infinitt_db_server = ""
# name of the db instance
infinitt_db_name = "spectra"

# Like \\\\192.168.2.10\\backup10\\
network_share_root = "\\\\192.168.2.10\\backup10\\"

versioning_suffix = ".version"

query = """Select
    STUDY.STUDY_KEY,
    STUDY.PATIENT_ID,
    INSTANCELOC.PATHNAME,
    STUDY.STUDY_DTTM
From
    STUDY Inner Join
    INSTANCE On INSTANCE.STUDY_KEY = STUDY.STUDY_KEY Inner Join
    INSTANCELOC On INSTANCELOC.INSTANCE_KEY = INSTANCE.INSTANCE_KEY
Where
    STUDY.STUDY_DESC Like 'NUK Myo%' And
    To_Char(STUDY.STUDY_DTTM, 'DD.MM.YYYY') Like '%.2015' And
    INSTANCELOC.B2_VOL_CODE = 'B1' And
    STUDY.STUDY_DTTM >= '01.04.2015'
GROUP BY
    STUDY.STUDY_KEY,
    study.PATIENT_ID,
    INSTANCELOC.PATHNAME,
    study.STUDY_DTTM
"""
