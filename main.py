# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import config
import csv
import logging
import subprocess
from logging.handlers import RotatingFileHandler
from os import listdir
from os.path import isfile, join
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

con_string = "oracle://{}:{}@{}:1521/{}".format(config.infinitt_db_user,
                                                config.infinitt_db_pw,
                                                config.infinitt_db_server,
                                                config.infinitt_db_name)
# Connect to database
ora_engine = create_engine(con_string, coerce_to_unicode=False)
Ora_Session = scoped_session(sessionmaker(autocommit=True,
                                         autoflush=True,
                                         bind=ora_engine))

logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler('log.log',
                              maxBytes=1000000,
                              backupCount=10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [DiagMyoFix] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

processed_studies = []


def get_myos():
    ora_con = Ora_Session()
    db_myocards = ora_con.execute(config.query)
    myocards = []
    for myo in db_myocards:
        myocards.append(myo)
    ora_con.close()
    ora_con = None
    return myocards


def get_files(path):
    return [f for f in listdir(path) if isfile(join(path, f))]


def change_version(file):
    try:
        logger.info(f"Changing version of file {file}")
        params = f"echo switch 0 > {file}{config.versioning_suffix}"
        proc = subprocess.Popen(params, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        logger.info(f"File processed. Output was: {proc.communicate()}")
        return True
    except Exception as e:
        logger.exception(e)
        return False


def process_myocards(myocards):
    logger.info("Processing Myocards")
    for myo in myocards:
        myo_dir = join(config.network_share_root, myo[2])
        logger.info(f"Process: Study: {myo[0]} PID: {myo[1]} DIR: {myo_dir} Date: {myo[3]}")
        files = get_files(myo_dir)
        error = False
        for file in files:
            if "mig" in file.lower() or "bak" in file.lower():
                logger.info(f"Ignoring backup file {file}")
            else:
                processed = change_version(join(myo_dir, file))
                if not processed:
                    error = True
        processed_studies.append([myo[2].split("/")[1], myo[0], myo[1], myo[2].split("/")[0], error])


def main():
    logger.info("Script started.")
    logger.info("Getting Myocards")
    myocards = get_myos()
    logger.info(f"Found {len(myocards)} Myocards")
    process_myocards(myocards=myocards)
    logger.info(f"Processing done. Processed {len(processed_studies)} studies.")
    with open("processed_studies.csv", "w") as f:
        writer = csv.writer(f, delimiter=",", lineterminator="\n")
        writer.writerow(['E', 'Study', 'PID', 'Date', 'Error'])
        writer.writerows(processed_studies)
    logger.info("Written result to CSV. Done.")


if __name__ == '__main__':
    main()
